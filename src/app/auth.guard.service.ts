import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { LoginService } from './services/login.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private loginService: LoginService, private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.loginService.isLoggedIn()) {
            return true;
        }
        this.router.navigate([''])
        return false;
    }
}

