import { Component, OnInit } from '@angular/core';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';
import { OcorrenciasService } from '../services/ocorrenciasService';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-cadastro-ocorrencia',
  templateUrl: './cadastro-ocorrencia.component.html',
  styleUrls: ['./cadastro-ocorrencia.component.css']
})
export class CadastroOcorrenciaComponent implements OnInit {
  protected listOcorrencias = [];
  protected status: string = "Em Aberto"
  protected load: boolean = false;
  protected data: IAuth
  protected formOcorrencia: FormGroup;
  protected cadastrar: boolean = false;
  protected responder: boolean = false;
  protected reabrir: boolean = false;
  protected idOcorrencia: any;
  protected messageOut: any;
  protected user: any;
  constructor(private login: LoginService, private ocorrenciaService: OcorrenciasService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.data = this.login.getLogged();
    this.user = this.data.data[0].tipo;
    this.loadTabs();
    this.initForm();
  }

  protected loadTabs(): void {
    this.load = true
    this.ocorrenciaService.getOcorrencias(this.data.data[0].cnpj, this.data.data[0].tipo, this.data.data[0].login, this.status).subscribe((result) => {
      this.listOcorrencias = result.lista
      this.load = false
    }, error => {
      this.load = false
      console.log(error)
    })
  }

  private initForm(): void {
    this.formOcorrencia = this.formBuilder.group({
      titulo: ["", Validators.required],
      mensagem_in: ["", Validators.required],
    })
  }

  protected doSalvar(): void {
    const validator = utilValidators.validatorAllForms(this.formOcorrencia);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "titulo": this.formOcorrencia.get("titulo").value,
        "mensagem_in": this.formOcorrencia.get("mensagem_in").value,
        "mensagem_out": null,
        "status_ocorrencia": 'Em Aberto',
        "data_ocorrencia": moment().format("YYYY-MM-DD"),
        "login_usuario": this.data.data[0].login,
        "cnpj_condominio": this.data.data[0].cnpj,
      }
      console.log(params);
      this.ocorrenciaService.addOcorrencias(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formOcorrencia.reset();
          this.status = 'Em Aberto'
          this.loadTabs();
          this.cadastrar = false;
        }
      }, error => {
        this.load = false;
        this.cadastrar = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected encerrarOcorrencia(): void {
    if (this.idOcorrencia == '' || this.messageOut == '') {
      swal({ title: "", text: 'Favor preencher os campos!', type: 'error' });
      return
    }
    this.ocorrenciaService.encerrarOcorrencia(this.idOcorrencia, this.messageOut, 'Encerrado').subscribe((result) => {
      if (result) {
        this.load = false;
        swal({ title: "", text: result.message, type: 'success' });
        this.formOcorrencia.reset();
        this.status = 'Em Aberto'
        this.loadTabs();
        this.responder = false;
        this.messageOut = '';
        this.idOcorrencia = ''
      }
    }, error => {
      this.load = false;
      this.responder = false;
      this.messageOut = '';
      this.idOcorrencia = ''
      swal({ title: "", text: error.message, type: 'error' });
    })
  }

  protected reabrirOcorrencia(): void {
    this.ocorrenciaService.reabrirOcorrencia(this.idOcorrencia, 'Em Aberto').subscribe((result) => {
      if (result) {
        this.load = false;
        swal({ title: "", text: result.message, type: 'success' });
        this.formOcorrencia.reset();
        this.status = 'Em Aberto'
        this.loadTabs();
        this.reabrir = false;
        this.messageOut = '';
        this.idOcorrencia = ''
      }
    }, error => {
      this.load = false;
      this.reabrir = false;
      this.messageOut = '';
      this.idOcorrencia = ''
      swal({ title: "", text: error.message, type: 'error' });
    })
  }
}
