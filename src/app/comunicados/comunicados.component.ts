import { Component, OnInit } from '@angular/core';
import { ComunicadosService } from '../services/comunicados.service';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';
import swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-comunicados',
  templateUrl: './comunicados.component.html',
  styleUrls: ['./comunicados.component.css']
})
export class ComunicadosComponent implements OnInit {
  protected listOcorrencia = [];
  private data: IAuth;
  protected load: boolean = false;
  protected user: any;
  constructor(private ocorrenciaService: ComunicadosService, private loginService: LoginService) { }

  ngOnInit() {
    this.data = this.loginService.getLogged();
    this.user = this.data.data[0].tipo;
    this.getList();
  }

  private getList(): void {
    this.load = true;
    this.ocorrenciaService.getOcorrencia(this.data.data[0].cnpj).subscribe((result) => {
      this.listOcorrencia = result.lista;
      this.load = false;
    })
  }

  protected deleteComunicado(id): void {
    this.load = true;
    this.ocorrenciaService.deleteOcorrencia(id).subscribe((result) => {
      this.load = false;
      swal({ title: "", text: result.message, type: 'success' });
      this.getList();
    }, error => {
      this.load = false;
      swal({ title: "", text: error.message, type: 'error' });
    })
  }

}
