import { Component, OnInit, Inject } from '@angular/core';
import { Options } from 'fullcalendar';
import * as moment from "moment";
import { ReservasService } from '../services/reservas.service';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';
import swal from 'sweetalert2';

@Component({
    selector: 'app-reservas',
    templateUrl: './reservas.component.html',
    styleUrls: ['./reservas.component.css']
})
export class ReservasComponent implements OnInit {
    protected calendarOptions: Options;
    protected data: IAuth;
    protected events = [];
    protected listAreas = [];
    protected isOpen = false;
    protected dateSelected: any;
    protected dataSelectParam: any;
    protected load = false;
    protected idArea: any;
    protected isEvent: boolean = false;
    protected horario_ini: any = null;
    protected horario_fim: any = null;
    protected item: string;
    constructor(private reservasService: ReservasService, private login: LoginService) { }

    ngOnInit() {
        this.data = this.login.getLogged();
        this.loadAreas();
        this.loadEvents();
        this.load = true;

    }

    private loadEvents(): void {
        this.reservasService.getReservas(this.data.data[0].cnpj).subscribe((result) => {
            result.lista.forEach(element => {
                let param = {
                    title: element.nome,
                    start: moment(`${element.data_ini_reserva} ${element.horario_ini != null ? element.horario_ini : ''}`),
                    end: moment(`${element.data_fim_reserva} ${element.data_fim_reserva != null ? element.data_fim_reserva : ''}`),
                    allDay: element.horario_ini != null ? false : true
                }
                this.events.push(param);
            });

            this.calendarOptions = {
                editable: false,
                eventLimit: true,
                locale: 'pt',
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
                events: this.events,
                validRange: {
                    start: moment().subtract(1, 'day').toString(),
                    end: moment().add(1, 'year').toString()
                },
            };
            this.load = false;
            this.isEvent = true;
        });
    }

    private loadAreas(): void {
        this.reservasService.getAreas(this.data.data[0].cnpj).subscribe((result) => {
            this.listAreas = result.lista
        })
    }

    protected eventClick(ev): void {

        this.dataSelectParam = moment(ev).add(1, 'day');
        this.dateSelected = moment(ev).add(1, 'day').format("DD-MM-YYYY").toString()
        this.isOpen = true;
    }

    protected cadastraReserva(): void {
        this.load = true;
        let params = {
            "data_ini_reserva": moment(this.dataSelectParam).format("YYYY-MM-DD"),
            "data_fim_reserva": moment(this.dataSelectParam).format("YYYY-MM-DD"),
            "id_area": this.idArea.split("-")[0],
            "login_usuario": this.data.data[0].login,
            "cnpj_condominio": this.data.data[0].cnpj,
            "horario_ini": this.horario_ini != null ? this.horario_ini + ':00' : null,
            "horario_fim": this.horario_fim != null ? this.horario_fim + ':00' : null
        }

        this.reservasService.addReserva(params).subscribe(result => {
            if (result) {
                this.load = false;
                this.isOpen = false;
                this.isEvent = false;
                this.events = [];
                this.loadEvents();
                swal({ title: "", text: result.message, type: 'success' });

            } else {

                this.load = false;
                this.isOpen = false;
                this.isEvent = false;
                this.events = [];
                this.loadEvents();
                swal({ title: "", text: result.message, type: 'warning' });

            }
        }, error => {
            this.load = false;
            this.isOpen = false;
            this.loadEvents();
            swal({ title: "", text: error.message, type: 'error' });
        })
    }
}


