import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})
export class CadastroUsuarioComponent implements OnInit {
  protected listTipo = [];
  protected listCondomino = [];
  protected formCadastroUsuario: FormGroup;
  protected load: boolean = false;
  constructor(private usuarioService: UsuarioService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.usuarioService.getListTipo().subscribe((result) => {
      this.listTipo = result.lista
    })
    this.usuarioService.getListCondominio().subscribe((result) => {
      this.listCondomino = result.lista
    })
    this.initForm();
  }

  private initForm(): void {
    this.formCadastroUsuario = this.formBuilder.group({
      login: ["", Validators.required],
      senha: ["", Validators.required],
      tipo: ["", Validators.required],
      cpf_pessoa: ["", Validators.required]
    })
  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroUsuario);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "login": this.formCadastroUsuario.get("login").value,
        "senha": this.formCadastroUsuario.get("senha").value,
        "status_usuario": "A",
        "tipo": Number(this.formCadastroUsuario.get("tipo").value),
        "data_cadastro": moment().format("YYYY-MM-DD"),
        "data_alteracao": moment().format("YYYY-MM-DD"),
        "cpf_pessoa": this.formCadastroUsuario.get("cpf_pessoa").value,
      }
      this.usuarioService.addUser(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroUsuario.reset();
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected clear(): void {
    this.initForm();
  }
}
