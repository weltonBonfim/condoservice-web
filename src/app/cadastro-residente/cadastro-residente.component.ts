import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import * as moment from "moment";
import { utilValidators } from '../services/utilValidator';
import { ResidenteService } from '../services/residente.service';
import { LoginService } from '../services/login.service';
import { IAuth } from '../interfaces/IAuth';

@Component({
  selector: 'app-cadastro-residente',
  templateUrl: './cadastro-residente.component.html',
  styleUrls: ['./cadastro-residente.component.css']
})
export class CadastroResidenteComponent implements OnInit {
  protected listApto = [];
  protected listCondominio = [];
  protected formCadastroResidente: FormGroup;
  protected load: boolean = false;
  protected data: IAuth;
  constructor(private residenteService: ResidenteService, private formBuilder: FormBuilder, private login: LoginService) { }

  ngOnInit() {
    this.data = this.login.getLogged()
    this.loadListaApto();
    this.loadListaCondominio();
    this.initForm();
  }

  protected loadListaApto(): void {
    this.residenteService.getApto(this.data.data[0].cnpj).subscribe(result => {
      this.listApto = result
    }, error => {
      console.log(error)
    })
  }

  protected loadListaCondominio(): void {
    this.residenteService.getCondominios().subscribe(result => {
      this.listCondominio = result.filter((result) => {
        return result.cnpj == this.data.data[0].cnpj
      })
    }, error => {
      console.log(error)
    })
  }

  protected changeCep(ev): void {
    if (ev.target.value.length === 8) {
      this.load = true;
      this.residenteService.getCep(ev.target.value).subscribe(result => {
        this.formCadastroResidente.get("endereco").setValue(result.logradouro);
        this.formCadastroResidente.get("cidade").setValue(result.localidade);
        this.formCadastroResidente.get("estado").setValue(result.uf);
        this.formCadastroResidente.get("bairro").setValue(result.bairro);
        this.load = false;
      }, error => {
        this.load = false;
      })
    }
  }

  private initForm(): void {
    this.formCadastroResidente = this.formBuilder.group({
      cpf: ["", Validators.compose([Validators.required])],
      rg: ["", Validators.required],
      nome: ["", Validators.required],
      data_nascimento: ["", Validators.required],
      cep: ["", Validators.required],
      endereco: ["", Validators.required],
      numero: ["", Validators.required],
      bairro: ["", Validators.required],
      cidade: ["", Validators.required],
      estado: ["", Validators.required],
      telefone: ["", Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      cnpj_condominio: ["", Validators.required],
      apto: ["", Validators.required]
    })
  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroResidente);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "cpf": this.formCadastroResidente.get("cpf").value,
        "rg": this.formCadastroResidente.get("rg").value,
        "nome": this.formCadastroResidente.get("nome").value,
        "data_nascimento": this.formCadastroResidente.get("data_nascimento").value,
        "cep": this.formCadastroResidente.get("cep").value,
        "endereco": this.formCadastroResidente.get("endereco").value,
        "numero": this.formCadastroResidente.get("numero").value,
        "bairro": this.formCadastroResidente.get("cidade").value,
        "cidade": this.formCadastroResidente.get("cidade").value,
        "estado": this.formCadastroResidente.get("bairro").value,
        "telefone": this.formCadastroResidente.get("telefone").value,
        "email": this.formCadastroResidente.get("email").value,
        "data_cadastro": moment().format("YYYY-MM-DD"),
        "cnpj_condominio": this.formCadastroResidente.get("cnpj_condominio").value,
        "id_apto": this.formCadastroResidente.get("apto").value,
      }

      this.residenteService.cadastraResidente(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroResidente.reset();
          this.loadListaApto();
          this.loadListaCondominio();
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected clear(): void {
    this.initForm();
  }

}
