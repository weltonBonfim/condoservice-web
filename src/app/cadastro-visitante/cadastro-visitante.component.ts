import { Component, OnInit } from '@angular/core';
import { IAuth } from '../interfaces/IAuth';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { VisitantesService } from '../services/visitantes.service';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-cadastro-visitante',
  templateUrl: './cadastro-visitante.component.html',
  styleUrls: ['./cadastro-visitante.component.css']
})
export class CadastroVisitanteComponent implements OnInit {
  protected listApto = [];
  protected listVisitantes = [];
  protected listRegistro = [];
  protected formCadastroVisitante: FormGroup;
  protected load: boolean = false;
  protected data: IAuth;
  protected cadastrar: boolean = false;
  constructor(private login: LoginService, private visitantesService: VisitantesService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.data = this.login.getLogged()
    this.loadListaApto();
    this.loadListVisitantes();
    this.loadRegistros();
    this.initForm();
  }

  protected loadListaApto(): void {
    this.visitantesService.getApto(this.data.data[0].cnpj).subscribe(result => {
      this.listApto = result
    }, error => {
      console.log(error)
    })
  }

  protected loadListVisitantes(): void {
    this.visitantesService.getVisitantes(this.data.data[0].cnpj).subscribe(result => {
      this.listVisitantes = result.lista
    }, error => {
      console.log(error)
    })
  }

  protected loadRegistros(): void {
    this.visitantesService.getRegistro(this.data.data[0].cnpj, moment().format("YYYY-MM-DD")).subscribe((result) => {
      this.listRegistro = result.lista
    })
  }

  private initForm(): void {
    this.formCadastroVisitante = this.formBuilder.group({
      doc_visitante: ["", Validators.required],
      nome_visitante: ["", Validators.required],
      motivo_visita: ["", Validators.required],
      id_apto: ["", Validators.required],
    })
  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroVisitante);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "nome_visitante": this.formCadastroVisitante.get("nome_visitante").value,
        "motivo_visita": this.formCadastroVisitante.get("motivo_visita").value,
        "data_entrada": moment().format("YYYY-MM-DD"),
        "data_saida": null,
        "id_apto": Number(this.formCadastroVisitante.get("id_apto").value),
        "cnpj_condominio": this.data.data[0].cnpj,
        "doc_visitante": this.formCadastroVisitante.get("doc_visitante").value,
      }

      this.visitantesService.cadastroVisitante(params).subscribe(result => {
        if (result) {
          this.load = false;
          this.cadastrar = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroVisitante.reset();
          this.loadListaApto();
          this.loadListVisitantes();
          this.loadRegistros();
        }
      }, error => {
        console.log(error)
        this.load = false;
        this.cadastrar = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected registraSaida(id): void {
    this.load = true;
    this.visitantesService.registroSaida(id, moment().format("YYYY-MM-DD")).subscribe((result) => {
      if (result.status) {
        this.load = false;
        swal({ title: "", text: result.message, type: 'success' });
        this.loadListaApto();
        this.loadListVisitantes();
        this.loadRegistros();
      }
    }, error => {
      this.load = false;
      swal({ title: "", text: error.message, type: 'error' });
    })
  }
}
