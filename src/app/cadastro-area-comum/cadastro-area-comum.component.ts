import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import { AreaComumService } from '../services/areaComum.service';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-cadastro-area-comum',
  templateUrl: './cadastro-area-comum.component.html',
  styleUrls: ['./cadastro-area-comum.component.css']
})
export class CadastroAreaComumComponent implements OnInit {

  protected listCondominio: any;
  protected formCadastroArea: FormGroup;
  protected load: boolean = false;
  protected data: IAuth

  constructor(private areaComumService: AreaComumService, private formBuilder: FormBuilder, private login: LoginService) { }

  ngOnInit() {
    this.data = this.login.getLogged()
    this.loadListaCondominio();
    this.initForm();
  }

  protected loadListaCondominio(): void {
    this.areaComumService.getCondominios().subscribe(result => {
      this.listCondominio = result.filter((result) => {
        return result.cnpj == this.data.data[0].cnpj
      })
    })
  }

  private initForm(): void {
    this.formCadastroArea = this.formBuilder.group({
      nome: ["", Validators.required],
      descricao: ["", Validators.required],
      valor: ["", Validators.required],
      detalhes: ["", Validators.required],
      limite_horario: ["", Validators.required],
      limite_pessoas: ["", Validators.required],
      cnpj_condominio: ["", Validators.required],
      tipo_espaco: ["", Validators.required]
    })
  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroArea);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "nome": this.formCadastroArea.get("nome").value,
        "descricao": this.formCadastroArea.get("descricao").value,
        "valor": this.formCadastroArea.get("valor").value,
        "detalhes": this.formCadastroArea.get("detalhes").value,
        "limite_horario": this.formCadastroArea.get("limite_horario").value + ":00",
        "limite_pessoas": this.formCadastroArea.get("limite_pessoas").value,
        "cnpj_condominio": this.formCadastroArea.get("cnpj_condominio").value,
        "tipo_espaco": this.formCadastroArea.get("tipo_espaco").value
      }
      this.areaComumService.cadastraAreaComum(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroArea.reset();
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected clear(): void {
    this.initForm();
  }

}
