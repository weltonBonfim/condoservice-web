import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import * as moment from "moment";
import { ComunicadosService } from '../services/comunicados.service';
import { utilValidators } from '../services/utilValidator';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-cadastro-comunicados',
  templateUrl: './cadastro-comunicados.component.html',
  styleUrls: ['./cadastro-comunicados.component.css']
})
export class CadastroComunicadosComponent implements OnInit {
  protected formOcorrencia: FormGroup;
  protected load: boolean = false;
  private data: IAuth;
  protected descricao: any;
  protected editorConfig: any;
  constructor(private formBuilder: FormBuilder, private ocorrenciaService: ComunicadosService, private loginService: LoginService) { }

  ngOnInit() {
    this.data = this.loginService.getLogged();
    this.initForm();
    this.editorConfig = {
      height: 200,
      toolbarCanCollapse: true,
      toolbarGroups: [
        { name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'insert', groups: ['insert'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'links', groups: ['links'] },
        { name: 'tools', groups: ['tools'] },
        { name: 'colors', groups: ['colors'] },
        '/',
        { name: 'styles', groups: ['styles'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] }
      ],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Save,NewPage,Preview,Image,Flash,Smiley,Iframe,Unlink,Link,About,Templates,Anchor,Language'
    };
  }

  public options: Object = {
    placeholderText: 'Informe o texto do seu documento aqui!',
    charCounterCount: false
  }


  private initForm(): void {
    this.formOcorrencia = this.formBuilder.group({
      titulo: ["", Validators.required]
    })
  }

  protected doSalvar(): void {
    const validator = utilValidators.validatorAllForms(this.formOcorrencia);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "titulo": this.formOcorrencia.get("titulo").value,
        "descricao": this.descricao,
        "data_ocorrencia": moment().format("YYYY-MM-DD"),
        "nome_usuario": this.data.data[0].nome,
        "cnpj_condominio": this.data.data[0].cnpj,
      }
      console.log(params);
      this.ocorrenciaService.addOcorrencia(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formOcorrencia.reset();
          this.descricao = "";
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }



}
