import { Component, OnInit, ElementRef } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { LoginService } from '../../services/login.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    location: Location;

    constructor(location: Location, private loginService: LoginService) {
        this.location = location;
    }

    ngOnInit() {

    }

    getTitle() {

    }

    protected logout(): void {
        this.loginService.logout()
    }
}
