import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { IAuth } from '../../interfaces/IAuth';
declare const $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    private open = false;
    protected portaria = false;
    protected comunicados = false;
    protected configuracoes = false;
    protected user: any;
    protected data: IAuth;

    constructor(private login: LoginService) { }

    ngOnInit() {
        this.data = this.login.getLogged();
        this.user = this.data.data[0].tipo
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    protected openMenuPortaria(menu: string): void {
        if (menu == "portaria" && this.open == false) {
            this.portaria = true
            this.open = true;
        } else {
            this.portaria = false;
            this.open = false;
        }


    }
    protected openMenuConfiguracoes(menu: string): void {
        if (menu == "configuracoes" && this.open == false) {
            this.configuracoes = true;
            this.open = true;
        } else {
            this.configuracoes = false;
            this.open = false;
        }
    }

    protected openMenuComunicados(menu: string): void {
        if (menu == "comunicados" && this.open == false) {
            this.comunicados = true;
            this.open = true;
        } else {
            this.comunicados = false;
            this.open = false;
        }
    }
}
