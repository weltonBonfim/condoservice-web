import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-cadastro-condomninio',
  templateUrl: './cadastro-condomninio.component.html',
  styleUrls: ['./cadastro-condomninio.component.css']
})
export class CadastroCondomninioComponent implements OnInit {

  protected listCondominio: any;
  protected formCadastroCondominio: FormGroup;
  protected load: boolean = false;
  constructor(private dashboardService: DashboardService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loadListaCondominio();
    this.initForm();
  }

  protected loadListaCondominio(): void {
    this.dashboardService.getCondominios().subscribe(result => {
      this.listCondominio = result
    })
  }

  protected changeCep(ev): void {
    if (ev.target.value.length === 8) {
      this.load = true;
      this.dashboardService.getCep(ev.target.value).subscribe(result => {
        this.formCadastroCondominio.get("endereco").setValue(result.logradouro);
        this.formCadastroCondominio.get("cidade").setValue(result.localidade);
        this.formCadastroCondominio.get("estado").setValue(result.uf);
        this.formCadastroCondominio.get("bairro").setValue(result.bairro);
        this.load = false;
      }, error => {
        this.load = false;
      })
    }
  }

  private initForm(): void {
    this.formCadastroCondominio = this.formBuilder.group({
      cnpj: ["", Validators.compose([Validators.required, utilValidators.validatorCnpj])],
      razao_social: ["", Validators.required],
      inscricao_estadual: ["", Validators.required],
      situacao: ["", Validators.required],
      cep: ["", Validators.required],
      endereco: ["", Validators.required],
      numero: ["", Validators.required],
      bairro: ["", Validators.required],
      cidade: ["", Validators.required],
      estado: ["", Validators.required],
      telefone: ["", Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.email])],
    })


  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroCondominio);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "bairro": this.formCadastroCondominio.get("bairro").value,
        "cep": this.formCadastroCondominio.get("cep").value,
        "cidade": this.formCadastroCondominio.get("cidade").value,
        "cnpj": this.formCadastroCondominio.get("cnpj").value,
        "data_cadastro": moment().format("YYYY-MM-DD"),
        "email": this.formCadastroCondominio.get("email").value,
        "endereco": this.formCadastroCondominio.get("endereco").value,
        "estado": this.formCadastroCondominio.get("estado").value,
        "inscricao_estadual": this.formCadastroCondominio.get("inscricao_estadual").value,
        "numero": this.formCadastroCondominio.get("numero").value,
        "razao_social": this.formCadastroCondominio.get("razao_social").value,
        "situacao": this.formCadastroCondominio.get("situacao").value,
        "telefone": this.formCadastroCondominio.get("telefone").value
      }
      this.dashboardService.cadastraCondominio(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroCondominio.reset();
          this.loadListaCondominio();
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected clear(): void {
    this.initForm();
  }

}
