import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatTooltipModule,
  MatCardModule,
  MatFormFieldModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatDialogModule,
} from '@angular/material';
import { LoginService } from './services/login.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth.guard.service';
import { DashboardService } from './services/dashboard.service';
import { DataPipe } from './data.pipe';
import { CadastroCondomninioComponent } from './cadastro-condomninio/cadastro-condomninio.component';
import { CadastroAptoComponent } from './cadastro-apto/cadastro-apto.component';
import { AptoService } from './services/apto.service';
import { CadastroResidenteComponent } from './cadastro-residente/cadastro-residente.component';
import { ResidenteService } from './services/residente.service';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { CadastroAreaComumComponent } from './cadastro-area-comum/cadastro-area-comum.component';
import { UsuarioService } from './services/usuario.service';
import { CadastroComunicadosComponent } from './cadastro-comunicados/cadastro-comunicados.component';
import { ComunicadosService } from './services/comunicados.service';
import { ComunicadosComponent } from './comunicados/comunicados.component';
import { CKEditorModule } from 'ngx-ckeditor';
import { CadastroVisitanteComponent } from './cadastro-visitante/cadastro-visitante.component';
import { CadastroOcorrenciaComponent } from './cadastro-ocorrencia/cadastro-ocorrencia.component';
import { AreaComumService } from './services/areaComum.service';
import { VisitantesService } from './services/visitantes.service';
import { OcorrenciasService } from './services/ocorrenciasService';
import { ReservasComponent } from './reservas/reservas.component';
import { FullCalendarModule } from 'ng-fullcalendar';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { ReservasService } from './services/reservas.service';
import { AtivarRfidComponent } from './ativar-rfid/ativar-rfid.component';
import { RFIDService } from './services/rfid.service';

registerLocaleData(localePt);

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MatButtonModule,
    MatRippleModule,
    MatInputModule,
    MatTooltipModule,
    MatCardModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatDialogModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    CKEditorModule,
    FullCalendarModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    DataPipe,
    CadastroCondomninioComponent,
    CadastroAptoComponent,
    CadastroResidenteComponent,
    CadastroUsuarioComponent,
    CadastroAreaComumComponent,
    CadastroComunicadosComponent,
    ComunicadosComponent,
    CadastroVisitanteComponent,
    CadastroOcorrenciaComponent,
    ReservasComponent,
    AtivarRfidComponent,
  ],
  providers: [
    LoginService,
    DashboardService,
    AptoService,
    ResidenteService,
    UsuarioService,
    ComunicadosService,
    AreaComumService,
    VisitantesService,
    OcorrenciasService,
    ReservasService,
    AuthGuardService,
    RFIDService,
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
