import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = "/rest-api/rest/";

@Injectable()
export class AptoService {
    constructor(private http: HttpClient, ) {
    }

    public getCondominios(): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}condominio/list`, { headers: header })
    }

    public cadastraApto(params): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}apto/add`, params, { headers: header })
    }
}
