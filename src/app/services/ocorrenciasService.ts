import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
const URL = "/rest-api/rest/"

@Injectable()
export class OcorrenciasService {
    constructor(private http: HttpClient) {
    }

    public getOcorrencias(cnpj, tipo, login, status): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}ocorrencias/list/${cnpj}/${tipo}/${login}/${status}`, { headers: header })
    }

    public addOcorrencias(param): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}ocorrencias/add`, param, { headers: header })
    }

    public encerrarOcorrencia(id: any, message: any, status: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.put(`${URL}ocorrencias/encerrar-ocorrencia/${id}/${message}/${status}`, {}, { headers: header })
    }

    public reabrirOcorrencia(id: any, status: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.put(`${URL}ocorrencias/reabrir-ocorrencia/${id}/${status}`, {}, { headers: header })
    }
}  