import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = "/rest-api/rest/";
const URL_CEP = 'https://viacep.com.br/ws/';

@Injectable()
export class DashboardService {
    constructor(private http: HttpClient, ) {
    }

    public getCondominios(): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}condominio/list`, { headers: header })
    }

    public getCep(cep): Observable<any> {
        let header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL_CEP}${cep}/json`, { headers: header })
    }

    public cadastraCondominio(params): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}condominio/add`, params, { headers: header })
    }

    public updatePrimeiroAcesso(login: any, senha: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.put(`${URL}user/primeiro-acesso/${login}/${senha}`, {}, { headers: header })
    }
}
