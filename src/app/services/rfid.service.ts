
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = "/rest-api/rest/";

@Injectable()
export class RFIDService {
    constructor(private http: HttpClient, ) {
    }

    public ativarRFID(id: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.put(`${URL}rfid/ativa-rfid/${id}`, {}, { headers: header })
    }

    public listaRfid(): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}rfid/lista`, { headers: header })
    }
}