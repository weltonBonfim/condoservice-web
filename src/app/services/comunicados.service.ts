import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
const URL = "/rest-api/rest/"

@Injectable()
export class ComunicadosService {
    constructor(private http: HttpClient) {
    }

    public addOcorrencia(params: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}comunicados/add`, params, { headers: header })
    }

    public getOcorrencia(cnpj: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}comunicados/list/${cnpj}`, { headers: header })
    }

    public deleteOcorrencia(id: any): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.delete(`${URL}comunicados/delete/${id}`, { headers: header })
    }
}    