import { FormGroup, FormControl, AbstractControl, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";

export class utilValidators {
    public static validatorAllForms(formGroup: FormGroup): { messageAlertError: string } {
        for (const key of Object.keys(formGroup.controls)) {
            const control = formGroup.get(key);

            if (control instanceof FormControl) {
                if (control.hasError("required")) {
                    return { messageAlertError: `${ErrorMessagesFormRequired[key]}` };
                } else if (control.errors) {
                    return { messageAlertError: `${ErrorMessagesFormCustom[key]}` };
                }
            } else if (control instanceof FormGroup) {
                return utilValidators.validatorAllForms(control);
            }
        }

        return null;
    }

    public static validatorCnpj = (value: any): ValidationErrors => {
        if (!utilValidators.isCnpj(value.value)) return { cnpj: true };
        if (!utilValidators.validateNumberEqual(value.value)) return { cnpj: true };

        const cnpj = value.value.replace(/\D/g, "");

        let b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

        for (var i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
        if (cnpj[12] != ((n %= 11) < 2 ? 0 : 11 - n)) return { cnpj: true };

        for (var j = 0, n2 = 0; j <= 12; n2 += cnpj[j] * b[j++]);
        if (cnpj[13] != ((n2 %= 11) < 2 ? 0 : 11 - n2)) return { cnpj: true };

        return null;
    };


    public static validatorEmail(nameControlError: string = 'email'): ValidatorFn {
        return (abstractFormControl: AbstractControl): ValidationErrors => {
            if (!abstractFormControl || !abstractFormControl.value) { return null; }

            const email = Validators.email(abstractFormControl);
            if (email) { return { [`${nameControlError}`]: true }; }
            else {
                let arr: [string] = abstractFormControl.value.split("@");
                if (arr[1].split(".").length > 1) return null;
                return { [`${nameControlError}`]: true };
            }
        };
    }

    private static isCnpj(value: string): boolean {
        if (value.replace(/\D/g, "").length === 14) {
            return true;
        } else {
            return false;
        }
    }

    private static isCpf(value: string): boolean {
        if (value.replace(/\D/g, "").length === 11) {
            return true;
        } else {
            return false;
        }
    }

    private static validateNumberEqual(numberToValidate: string) {
        const numberReplace = numberToValidate.replace(/\D/g, "");

        const numberEqual: string = numberReplace.substring(0, 1);
        let booleanEqualNumber: boolean = false;
        for (let index = 0; index < numberReplace.length; index++) {
            if (numberEqual !== numberReplace[index]) {
                booleanEqualNumber = true;
            }
        }

        return booleanEqualNumber;
    }
}

const ErrorMessagesFormRequired = {
    ["cnpj"]: "O campo CNPJ é de preenchimento obrigatório!",
    ["bairro"]: "O campo Bairro é de preenchimento obrigatório!",
    ["razao_social"]: "O campo Razão Social é de preenchimento obrigatório!",
    ["inscricao_estadual"]: "O campo Inscriçaõ Estadual é de preenchimento obrigatório!",
    ["situacao"]: "O campo Situação é de preenchimento obrigatório!",
    ["cep"]: "O campo CEP é de preenchimento obrigatório!",
    ["endereco"]: "O campo Endereço é de preenchimento obrigatório!",
    ["numero"]: "O campo Número é de preenchimento obrigatório!",
    ["cidade"]: "O campo Cidade é de preenchimento obrigatório!",
    ["estado"]: "O campo Estado é de preenchimento obrigatório!",
    ["email"]: "O campo Email é de preenchimento obrigatório!",
    ["telefone"]: "O campo Telefone é de preenchimento obrigatório!",
    ["bloco"]: "O campo Bloco é de preenchimento obrigatório!",
    ["cnpj_condominio"]: "O campo CNPJ é de preenchimento obrigatório!",
    ["andar"]: "O campo Andar é de preenchimento obrigatório!",

};

const ErrorMessagesFormCustom = {
    ["cnpj"]: 'O campo CNPJ está invalido!',
    ["email"]: 'O campo Email está invalido!',
}