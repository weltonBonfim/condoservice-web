import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
const URL = "/rest-api/rest/"

@Injectable()
export class ReservasService {
    constructor(private http: HttpClient) {
    }

    public getReservas(cnpj): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}reservas/list/${cnpj}`, { headers: header })
    }

    public addReserva(param): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}reservas/add`, param, { headers: header })
    }

    public getAreas(cnpj): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}area-comum/list-area/${cnpj}`, { headers: header })
    }

} 