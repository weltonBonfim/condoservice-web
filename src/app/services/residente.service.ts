import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = "/rest-api/rest/";
const URL_CEP = 'https://viacep.com.br/ws/';

@Injectable()
export class ResidenteService {
    constructor(private http: HttpClient, ) {
    }

    public getCondominios(): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}condominio/list`, { headers: header })
    }

    public getApto(cnpj): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}apto/list/${cnpj}`, { headers: header })
    }

    public getCep(cep): Observable<any> {
        let header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL_CEP}${cep}/json`, { headers: header })
    }

    public cadastraResidente(params): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}pessoas/add`, params, { headers: header })
    }
}