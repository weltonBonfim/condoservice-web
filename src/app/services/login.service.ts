import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { IAuth } from '../interfaces/IAuth';
const URL = "/rest-api/rest/"


@Injectable()
export class LoginService {
  constructor(private http: HttpClient, private router: Router) {
  }

  public getLogin(params: any): Observable<any> {
    let header = new HttpHeaders({
      "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
      "Content-Type": "application/json"
    });
    return this.http.post(`${URL}user/login`, params, { headers: header })
  }

  public isLoggedIn(): boolean {
    if (localStorage.getItem("LoggedIn") || localStorage.getItem("LoggedIn") != null) {
      return true
    } else {
      return false
    }
  }

  public getLogged() {
    return JSON.parse(localStorage.getItem("LoggedIn"));
  }

  public setLogged(logged: IAuth): void {
    localStorage.setItem("LoggedIn", JSON.stringify(logged))
  }

  public logout() {
    localStorage.removeItem("LoggedIn");
    localStorage.clear();
    this.router.navigate(['']);
  }

}
