import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = "/rest-api/rest/";

@Injectable()
export class VisitantesService {
    constructor(private http: HttpClient, ) {
    }

    public getApto(cnpj): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}apto/list/${cnpj}`, { headers: header })
    }

    public getVisitantes(cnpj): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}visitantes/list/${cnpj}`, { headers: header })
    }

    public cadastroVisitante(params): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.post(`${URL}visitantes/add`, params, { headers: header })
    }

    public registroSaida(id, data_saida): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.put(`${URL}visitantes/registrar-saida/${id}/${data_saida}`, {}, { headers: header })
    }

    public getRegistro(cnpj, data): Observable<any> {
        let header = new HttpHeaders({
            "Authorization": "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M",
            "Content-Type": "application/json"
        });
        return this.http.get(`${URL}visitantes/registros/${cnpj}/${data}`, { headers: header })
    }
}