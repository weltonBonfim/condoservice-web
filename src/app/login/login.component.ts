import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2'
import { IAuth } from '../interfaces/IAuth';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  protected login = {
    login: "",
    senha: ""
  }
  protected load: boolean = false;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  protected doLogin(): void {
    if (this.login.senha === "" || this.login.login === "") {
      swal({ title: "", text: "Favor informar Login e Senha!", type: 'warning' });
    } else {
      this.load = true;
      this.loginService.getLogin(this.login).subscribe((result: IAuth) => {
        if (result.status) {
          this.loginService.setLogged(result);
          this.router.navigate(['/dashboard'])
          this.load = true;
        } else {
          swal({ title: "", text: result.message, type: 'error' })
          this.load = false;
        }
      }, error => {
        this.load = false;
        console.log(error)
      })
    }
  }
}
