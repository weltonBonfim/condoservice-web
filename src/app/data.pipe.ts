import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'data'
})
export class DataPipe implements PipeTransform {

  transform(value: any): any {
    return `${value.split("-")[2]}/${value.split("-")[1]}/${value.split("-")[0]}`;
  }

}
