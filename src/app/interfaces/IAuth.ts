export interface IAuth {
    data: IData[];
    message: string;
    status: boolean;
}

export interface IData {
    tipo: string;
    andar: string;
    numero: string;
    auth: boolean;
    bloco: string;
    razao_social: string;
    cpf: string;
    nome: string;
    cnpj: string;
    login: string;
    primeiro_acesso: boolean;
}