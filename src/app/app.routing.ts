import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth.guard.service';
import { CadastroCondomninioComponent } from './cadastro-condomninio/cadastro-condomninio.component';
import { CadastroAptoComponent } from './cadastro-apto/cadastro-apto.component';
import { CadastroResidenteComponent } from './cadastro-residente/cadastro-residente.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { CadastroComunicadosComponent } from './cadastro-comunicados/cadastro-comunicados.component';
import { ComunicadosComponent } from './comunicados/comunicados.component';
import { CadastroAreaComumComponent } from './cadastro-area-comum/cadastro-area-comum.component';
import { CadastroVisitanteComponent } from './cadastro-visitante/cadastro-visitante.component';
import { CadastroOcorrenciaComponent } from './cadastro-ocorrencia/cadastro-ocorrencia.component';
import { ReservasComponent } from './reservas/reservas.component';
import { RFIDService } from './services/rfid.service';
import { AtivarRfidComponent } from './ativar-rfid/ativar-rfid.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full', },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-condominio', component: CadastroCondomninioComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-apto', component: CadastroAptoComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-residente', component: CadastroResidenteComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-usuario', component: CadastroUsuarioComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-comunicados', component: CadastroComunicadosComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-espacos', component: CadastroAreaComumComponent, canActivate: [AuthGuardService] },
  { path: 'cadastro-visitantes', component: CadastroVisitanteComponent, canActivate: [AuthGuardService] },
  { path: 'ocorrencias', component: CadastroOcorrenciaComponent, canActivate: [AuthGuardService] },
  { path: 'comunicados', component: ComunicadosComponent, canActivate: [AuthGuardService] },
  { path: 'reservas', component: ReservasComponent, canActivate: [AuthGuardService] },
  { path: 'ativar-rfid', component: AtivarRfidComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
