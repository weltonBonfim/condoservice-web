import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { IAuth } from '../interfaces/IAuth';
import { Router } from '@angular/router';
import { DashboardService } from '../services/dashboard.service';
import swal from 'sweetalert2';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    protected data: IAuth;
    protected isPrimeiroAcesso = false;
    protected senha: any;
    protected confirmaSenha: any;
    constructor(private login: LoginService, private router: Router, private dashboardService: DashboardService) { }

    ngOnInit() {
        this.data = this.login.getLogged();
        if (this.data.data[0].primeiro_acesso) {
            this.isPrimeiroAcesso = true;
        }
    }

    protected cancelar(): void {
        this.login.logout();
        this.router.navigate(['']);
    }

    protected atualizarSenha(): void {
        if (this.senha != this.confirmaSenha) {
            swal({ title: "", text: 'As senhas preenchidas não são iguais!', type: 'warning' });
            return
        }
        if (this.senha == '' || this.confirmaSenha == '') {
            swal({ title: "", text: 'Favor preencher todos os campos!', type: 'warning' });
            return
        }
        this.dashboardService.updatePrimeiroAcesso(this.data.data[0].login, this.senha).subscribe(() => {
            swal({ title: "", text: 'Senha alterada com sucesso!', type: 'success' });
            this.isPrimeiroAcesso = false
            this.data.data[0].primeiro_acesso = false;
        }, error => {
            swal({ title: "", text: 'Erro ao alterar senha tente novamente, mais tarde', type: 'warning' });
            this.cancelar();
        });
    }

}
