import { Component, OnInit } from '@angular/core';
import { RFIDService } from '../services/rfid.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { utilValidators } from '../services/utilValidator';
@Component({
    selector: 'app-ativar-rfid',
    templateUrl: './ativar-rfid.component.html',
    styleUrls: ['./ativar-rfid.component.css']
})
export class AtivarRfidComponent implements OnInit {

    constructor(private rfidService: RFIDService, private formBuilder: FormBuilder) { }

    protected listaRfid = [];
    protected load: boolean = false;
    protected formAtivar: FormGroup;

    ngOnInit() {
        this.load = true;
        this.rfidService.listaRfid().subscribe((result) => {
            this.listaRfid = result.lista;
            this.load = false;
        });
        this.initForm();
    }

    private initForm(): void {
        this.formAtivar = this.formBuilder.group({
            id_rfid: ["", Validators.required],
        })
    }

    protected doAtivar(): void {
        const validator = utilValidators.validatorAllForms(this.formAtivar);
        if (validator !== null) {
            swal({ title: "", text: validator.messageAlertError, type: 'warning' });
        } else {
            this.load = true;
            this.rfidService.ativarRFID(this.formAtivar.get("id_rfid").value).subscribe(result => {
                if (result) {
                    this.load = false;
                    swal({ title: "", text: result.message, type: 'success' });
                    this.formAtivar.reset();
                }
            }, error => {
                this.load = false;
                swal({ title: "", text: error.message, type: 'error' });
            })
        }
    }

    protected clear(): void {
        this.initForm();
    }
}
