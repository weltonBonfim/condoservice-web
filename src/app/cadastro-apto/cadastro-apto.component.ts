import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { utilValidators } from '../services/utilValidator';
import swal from 'sweetalert2';
import { AptoService } from '../services/apto.service';
import { IAuth } from '../interfaces/IAuth';
import { LoginService } from '../services/login.service';
@Component({
  selector: 'app-cadastro-apto',
  templateUrl: './cadastro-apto.component.html',
  styleUrls: ['./cadastro-apto.component.css']
})
export class CadastroAptoComponent implements OnInit {
  protected formCadastroApto: FormGroup;
  protected listCondominio = [];
  protected load: boolean = false;
  protected data: IAuth
  constructor(private aptoService: AptoService, private formBuilder: FormBuilder, private login: LoginService) { }

  ngOnInit() {
    this.data = this.login.getLogged()
    this.initForm();
    this.loadListaCondominio();
  }

  protected loadListaCondominio(): void {
    this.aptoService.getCondominios().subscribe(result => {
      this.listCondominio = result.filter((result) => {
        return result.cnpj == this.data.data[0].cnpj
      })
    })
  }

  private initForm(): void {
    this.formCadastroApto = this.formBuilder.group({
      numero: ["", Validators.required],
      bloco: ["", Validators.required],
      andar: ["", Validators.required],
      cnpj_condominio: ["", Validators.required],
    })
  }

  protected doCadastro(): void {
    const validator = utilValidators.validatorAllForms(this.formCadastroApto);
    if (validator !== null) {
      swal({ title: "", text: validator.messageAlertError, type: 'warning' });
    } else {
      this.load = true;
      let params = {
        "numero": this.formCadastroApto.get("numero").value,
        "bloco": this.formCadastroApto.get("bloco").value,
        "andar": this.formCadastroApto.get("andar").value,
        "cnpj_condominio": this.formCadastroApto.get("cnpj_condominio").value,
      }
      this.aptoService.cadastraApto(params).subscribe(result => {
        if (result) {
          this.load = false;
          swal({ title: "", text: result.message, type: 'success' });
          this.formCadastroApto.reset();
        }
      }, error => {
        this.load = false;
        swal({ title: "", text: error.message, type: 'error' });
      })
    }
  }

  protected clear(): void {
    this.initForm();
  }

}
